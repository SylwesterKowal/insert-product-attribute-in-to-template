<?php

namespace Kowal\InsertProductAttributeInToTemplate\Model\Config\Source;

class Textareas implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $collectionFactory;

    /** @var array */
    private $items;


    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {

        $collection = $this->getCollection();
        return $collection->getItems();
    }


    /**
     * @return array
     */
    private function getOptions()
    {
        $items = [];
        foreach ($this->getAttributes() as $attribute) {
            if ( $attribute->getFrontendInput() == 'weight' || $attribute->getFrontendInput() == 'price' || $attribute->getFrontendInput() == 'text' || $attribute->getFrontendInput() == 'textarea' && $attribute->getName() != 'description') {
//            if (strpos($attribute->getName(), 'description_') !== false) {
                $items[] = [
                    'label' => $attribute->getStoreLabel(), 'value' => $attribute->getName(),
                ];
            }
        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->collectionFactory->create();
    }
}

<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\InsertProductAttributeInToTemplate\Model\Config\Source;

class Size implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'product_page_main_image', 'label' => __('Product page main image')],
            ['value' => 'product_page_image_large', 'label' => __('Product page image large 1380x880')],
            ['value' => 'product_page_image_rectangle_large', 'label' => __('Product page image large 1380x1380')],
            ['value' => 'product_page_image_medium', 'label' => __('Product page image medium 635x635')],
            ['value' => 'product_page_image_small', 'label' => __('Product page image small')],
            ['value' => 'product_page_image_panorama', 'label' => __('Product page image Panorama 440x280')],
            ['value' => 'product_page_image_panorama_2col', 'label' => __('Product page image Panorama 635x404')],
            ['value' => 'product_page_image_panorama_large', 'label' => __('Product page image Panorama Large 1380x880')]
        ];
    }

    public function toArray()
    {
        return [
            'product_page_main_image' => __('Product page main image'),
            'product_page_image_large' => __('Product page image large'),
            'product_page_image_rectangle_large' => __('Product page image large'),
            'product_page_image_medium' => __('Product page image medium 635x635'),
            'product_page_image_small' => __('Product page image small'),
            'product_page_image_panorama' => __('Product page image panorama 3 col'),
            'product_page_image_panorama_2col' => __('Product page image panorama 2 col'),
            'product_page_image_panorama_large' => __('Product page image panorama large')
        ];
    }
}


<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\InsertProductAttributeInToTemplate\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Description extends Template implements BlockInterface
{

    protected $_template = "Kowal_InsertProductAttributeInToTemplate::widget/description.phtml";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context        $context,
        \Magento\Framework\Registry                             $registry,
        \Kowal\InsertProductAttributeInToTemplate\Helper\Render $render,
        \Magento\Cms\Model\Template\FilterProvider              $filterProvider,
        array                                                   $data = []
    )
    {
        $this->registry = $registry;
        $this->render = $render;
        $this->filterProvider = $filterProvider;
        parent::__construct($context, $data);
    }


    public function getDescription($attribute_code)
    {
        if ($product = $this->registry->registry('current_product')) {
            $attr = "get" . $this->render->camellSize($attribute_code);
            if ($description = $product->$attr()) {
                return $this->addWidget($description);
            } else {
                return "";
            }
        } else {
            return __("No product!");
        }
    }

    public function addWidget($description) {
        $newDescription = $this->filterProvider->getPageFilter()->filter(
            $description
        );
        return $newDescription;
    }

}


<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\InsertProductAttributeInToTemplate\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Image extends Template implements BlockInterface
{

    protected $_template = "widget/image.phtml";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context        $context,
        \Magento\Framework\Registry                             $registry,
        \Kowal\InsertProductAttributeInToTemplate\Helper\Render $render,
        \Magento\Catalog\Helper\Image                           $image,
        array                                                   $data = []
    )
    {
        $this->registry = $registry;
        $this->render = $render;
        $this->image = $image;
        parent::__construct($context, $data);
    }


    public function getImage($attribute_code)
    {
        if ($product = $this->registry->registry('current_product')) {

            $attr = "get" . $this->render->camellSize($attribute_code);
            $size = (empty($this->getData('size'))) ? 'product_page_main_image' : $this->getData('size');
            $size_fullscreen = (empty($this->getData('fullscreen'))) ? 'product_page_image_large' : $this->getData('fullscreen');

            if ($file = $product->$attr()) {
                if ($file != "no_selection" && $imageUrl = $this->image->init($product, $size)
                    ->setImageFile($file)
                    ->getUrl()) {

                    $alt = str_replace('"', "", $product->getName());

                    $img = ["url" => $imageUrl, "alt" => $alt, 'width'=>$this->image->getWidth(), 'height'=>$this->image->getHeight()];


                    $fullscreen = $this->image->init($product, $size_fullscreen)->setImageFile($file)->getUrl();
                    $img['fullscreen'] = ["url" => $fullscreen, "alt" => $alt, 'width'=>$this->image->getWidth(), 'height'=>$this->image->getHeight()];

                    return $img;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}


<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Mostviewed
 */


namespace Kowal\InsertProductAttributeInToTemplate\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Render extends AbstractHelper
{


    /**
     * @return string
     */
    public function camellSize($str, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        return $str;
    }
}
